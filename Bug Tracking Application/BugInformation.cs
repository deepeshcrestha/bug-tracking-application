﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ICSharpCode.TextEditor.Document;
using MySql.Data.MySqlClient;

namespace Bug_Tracking_Application
{
    public partial class BugInformation : Form
    {
        public Image image;
        MySqlConnection con = new MySqlConnection("server= localhost; port = 3306; database= bug_tracker; user id =root ; password = ;SslMode=none;convert zero datetime=True;");
        MySqlCommand cmd;
        MySqlDataAdapter adapt;
        Image file;
        DataTable dt, dtable;

        public enum UserType
        {
            Admin = 0,
            User = 1
        }

        //ID variable used in Updating and Deleting Record  
        int report_id = 0;

        //getting value from user dashboard and admin dashboard for auto completion of email and role
        public BugInformation(string str_email, string str_role)
        {
            InitializeComponent();
            txt_fDate.Text = DateTime.Now.ToString();
            txt_fEmail.Text = str_email;
            cmb_fRole.Text = str_role;
        }

        /// <summary>
        /// displaying data in datagridview and managing image size
        /// </summary>
        public void displayData()
        {
            string selectQuery = "select * from register_bug";
            cmd = new MySqlCommand(selectQuery, con);

            adapt = new MySqlDataAdapter(cmd);

            dt = new DataTable();
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView.RowTemplate.Height = 70;
            dataGridView.AllowUserToAddRows = false;

            adapt.Fill(dt);
            dataGridView.DataSource = dt;
            DataGridViewImageColumn imgcol = new DataGridViewImageColumn();
            imgcol = (DataGridViewImageColumn)dataGridView.Columns[12];
            imgcol.ImageLayout = DataGridViewImageCellLayout.Stretch;
            adapt.Dispose();
        }

        private void txt_email_TextChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// disabling button for users like developer/programmer/tester
        /// </summary>
        /// <param name="isEnabled"></param>
        private void setButtonEnabledDisabled(bool isEnabled)
        {
            btn_delete.Enabled = isEnabled;
        }

        //private bool isAdmin(int UserTypeId)
        //{
        //    return UserTypeId == (int)UserType.Admin;
        //}
        //if (session["Usertype"] == "admin")
        //{
        //    btn_delete.Visible = true;

        //}
        //else
        //{

        //    btn.Visible = false;
        //}

        //loading function displayData
        private void BugInformation_Load(object sender, EventArgs e)
        {

            displayData();

        }

        private void btn_Upload_Click(object sender, EventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();
            f.Filter = "JPG(*.JPG)|*.jpg";
            if (f.ShowDialog() == DialogResult.OK)
            {
                file = Image.FromFile(f.FileName);
                pictureBox.Image = file;
                lbl_image.Text = "Image Uploaded Successfully";
            }
        }


        public static byte[] converterDemo(Image x)
        {
            ImageConverter _imageConverter = new ImageConverter();
            byte[] xByte = (byte[])_imageConverter.ConvertTo(x, typeof(byte[]));
            return xByte;
        }

        //public void updateTable()
        //{

        //    var imagetobyte = converterDemo(file);

        //    cmd = new MySqlCommand("insert into fixed_bug (fixer_email, f_role, f_date, assignee_email, a_role, e_date, project_title, bug_id, severity, class, method, line, code, details, image) " +
        //        "values (@femail, @frole, @fdate, @email, @role, @date, @project_title, @bug_id, @severity, @class_name, @method_name, @line_number, @code_block, @bug_description, @image)", con);
        //    con.Open();
        //    cmd.Parameters.AddWithValue("@femail", txt_fEmail.Text);
        //    cmd.Parameters.AddWithValue("@frole", cmb_fRole.Text);
        //    cmd.Parameters.AddWithValue("@fdate", DateTime.Now.ToString("yyyy-MM-dd H:mm:ss"));
        //    cmd.Parameters.AddWithValue("@email", txt_email.Text);
        //    cmd.Parameters.AddWithValue("@role", cmb_role.Text);
        //    cmd.Parameters.AddWithValue("@date", this.dateTimePicker.Text);
        //    cmd.Parameters.AddWithValue("@project_title", txt_project.Text);
        //    cmd.Parameters.AddWithValue("@bug_id", txt_bugId.Text);
        //    cmd.Parameters.AddWithValue("@severity", cmb_severity.Text);
        //    cmd.Parameters.AddWithValue("@class_name", txt_className.Text);
        //    cmd.Parameters.AddWithValue("@method_name", txt_methodName.Text);
        //    cmd.Parameters.AddWithValue("@line_number", txt_lineNumber.Text);
        //    cmd.Parameters.AddWithValue("@code_block", txt_codeBlock.Text);
        //    cmd.Parameters.AddWithValue("@bug_description", txt_bugDetails.Text);
        //    var paramUserImage = new MySqlParameter("@image", MySqlDbType.Blob, imagetobyte.Length);
        //    paramUserImage.Value = imagetobyte;
        //    cmd.Parameters.Add(paramUserImage);
        //    updateHistory(Convert.ToInt32(txt_reportid.Text));

        //      //cmd.ExecuteNonQuery();
        //    int rowCount = cmd.ExecuteNonQuery();

        //    if (rowCount >= 1)
        //    {

        //    }
        //    MessageBox.Show("Record Updated Successfully.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Information);

        //    con.Close();

        //}


        /// <summary>
        /// this is used to convert byte to image
        /// </summary>
        /// <param name="blob"></param>
        /// <returns></returns>
        public static Bitmap ByteToImage(byte[] blob)
        {
            MemoryStream mStream = new MemoryStream();
            byte[] pData = blob;
            mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
            Bitmap bm = new Bitmap(mStream, false);
            mStream.Dispose();
            return bm;
        }

        //public void updateHistory(int report_id)
        //{
        //    var imagetobyte = converterDemo(file);
        //    cmd = new MySqlCommand("select * from register_bug where report_id=@report_id",con);

        //    cmd.Parameters.AddWithValue("@report_id", report_id);


        //    MySqlDataReader login = cmd.ExecuteReader();

        //    if (login.Read())
        //    {
        //       // int report_id = login.GetOrdinal("report_id");
        //        //String report_idforhistory = login.GetString(report_id);

        //        int email = login.GetOrdinal("email");
        //        String emailforhistory = login.GetString(email);

        //        int role = login.GetOrdinal("role");
        //        String roleforhistory = login.GetString(role);

        //        int entry_date = login.GetOrdinal("entry_date");
        //        String entry_dateforhistory = login.GetString(entry_date);

        //        int project_title = login.GetOrdinal("project_title");
        //        String project_titleforhistory = login.GetString(project_title);

        //        int bug_id = login.GetOrdinal("bug_id");
        //        String bug_idforhistory = login.GetString(bug_id);

        //        int severity = login.GetOrdinal("severity");
        //        String severityforhistory = login.GetString(severity);

        //        int class_name = login.GetOrdinal("class_name");
        //        String class_nameforhistory = login.GetString(class_name);

        //        int method_name = login.GetOrdinal("method_name");
        //        String method_nameforhistory = login.GetString(method_name);

        //        int line_number = login.GetOrdinal("line_number");
        //        String line_numberforhistory = login.GetString(line_number);

        //        int code_block = login.GetOrdinal("code_block");
        //        String code_blockforhistory = login.GetString(code_block);

        //        int bug_description = login.GetOrdinal("bug_description");
        //        String bug_descriptionforhistory = login.GetString(bug_description);

        //        int image = login.GetOrdinal("image");
        //        byte[] imageforhistory = (byte[])login.GetValue(image); 

        //        Bitmap img = ByteToImage(imageforhistory);

        //        con.Close();

        //        cmd = new MySqlCommand("insert into history values(report_id, email, role, entry_date, project_title, bug_id, severity, class_name, method_name, line_number, code_block, bug_description, image) " +
        //                "values (@report_id, @email, @role, @date, @project_title, @bug_id, @severity, @class_name, @method_name, @line_number, @code_block, @bug_description, @image)", con);
        //         con.Open();
        //        cmd.Parameters.AddWithValue("@report_id", report_id);
        //        cmd.Parameters.AddWithValue("@email", emailforhistory);
        //        cmd.Parameters.AddWithValue("@role", roleforhistory);
        //        cmd.Parameters.AddWithValue("@date", entry_dateforhistory);
        //        cmd.Parameters.AddWithValue("@project_title", project_titleforhistory);
        //        cmd.Parameters.AddWithValue("@bug_id", bug_idforhistory);
        //        cmd.Parameters.AddWithValue("@severity", severityforhistory);
        //        cmd.Parameters.AddWithValue("@class_name", class_nameforhistory);
        //        cmd.Parameters.AddWithValue("@method_name", method_nameforhistory);
        //        cmd.Parameters.AddWithValue("@line_number", line_numberforhistory);
        //        cmd.Parameters.AddWithValue("@code_block", code_blockforhistory);
        //        cmd.Parameters.AddWithValue("@bug_description", bug_descriptionforhistory);
        //        var paramUserImage = new MySqlParameter("@image", img);
        //        paramUserImage.Value = imagetobyte;
        //        cmd.Parameters.Add(paramUserImage);
        //        cmd.ExecuteNonQuery();
        //        con.Close();

        //    }
        //    else
        //    {
        //        MessageBox.Show("Error");
        //    }

        //}


        private void btn_update_Click(object sender, EventArgs e)
        {
            //  updateTable();

            var imagetobyte = converterDemo(file);
            if (txt_fEmail.Text != "" && cmb_fRole.Text != "" && txt_fDate.Text != "" && txt_email.Text != "" && cmb_role.Text != "" && this.dateTimePicker.Text != "" && txt_project.Text != "" && txt_bugId.Text != "" && cmb_severity.Text != "" && txt_bugDetails.Text != "")
            {
                cmd = new MySqlCommand("insert into fixed_bug (fixer_email, f_role, f_date, assignee_email, a_role, e_date, project_title, bug_id, severity, class, method, line, code, details, image) " +
                    "values (@femail, @frole, @fdate, @email, @role, @date, @project_title, @bug_id, @severity, @class_name, @method_name, @line_number, @code_block, @bug_description, @image)", con);
                con.Open();
                cmd.Parameters.AddWithValue("@femail", txt_fEmail.Text);
                cmd.Parameters.AddWithValue("@frole", cmb_fRole.Text);
                cmd.Parameters.AddWithValue("@fdate", DateTime.Now.ToString("yyyy-MM-dd H:mm:ss"));
                cmd.Parameters.AddWithValue("@email", txt_email.Text);
                cmd.Parameters.AddWithValue("@role", cmb_role.Text);
                cmd.Parameters.AddWithValue("@date", this.dateTimePicker.Text);
                cmd.Parameters.AddWithValue("@project_title", txt_project.Text);
                cmd.Parameters.AddWithValue("@bug_id", txt_bugId.Text);
                cmd.Parameters.AddWithValue("@severity", cmb_severity.Text);
                cmd.Parameters.AddWithValue("@class_name", txt_className.Text);
                cmd.Parameters.AddWithValue("@method_name", txt_methodName.Text);
                cmd.Parameters.AddWithValue("@line_number", txt_lineNumber.Text);
                cmd.Parameters.AddWithValue("@code_block", txt_codeBlock.Text);
                cmd.Parameters.AddWithValue("@bug_description", txt_bugDetails.Text);
                var paramUserImage = new MySqlParameter("@image", MySqlDbType.Blob, imagetobyte.Length);
                paramUserImage.Value = imagetobyte;
                cmd.Parameters.Add(paramUserImage);
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Record Updated Successfully.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Please Provide All Details.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Error);


            }
        }

        // deleting registered bug
        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (report_id != 0)
            {
                cmd = new MySqlCommand("delete from register_bug where report_id=@report_id", con);
                con.Open();
                cmd.Parameters.AddWithValue("@report_id", report_id);
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Record Deleted Successfully.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Information);
                displayData();
                //clearData();
            }
            else
            {
                MessageBox.Show("Please Select Record to Delete.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    


        //
        private void dataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            displayData();

            report_id = Convert.ToInt32(dataGridView.Rows[e.RowIndex].Cells[0].Value.ToString());
            txt_email.Text = dataGridView.Rows[e.RowIndex].Cells[1].Value.ToString();
            cmb_role.Text = dataGridView.Rows[e.RowIndex].Cells[2].Value.ToString();
            this.dateTimePicker.Text = dataGridView.Rows[e.RowIndex].Cells[3].Value.ToString();
            txt_project.Text = dataGridView.Rows[e.RowIndex].Cells[4].Value.ToString();
            txt_bugId.Text = dataGridView.Rows[e.RowIndex].Cells[5].Value.ToString();
            cmb_severity.Text = dataGridView.Rows[e.RowIndex].Cells[6].Value.ToString();
            txt_className.Text = dataGridView.Rows[e.RowIndex].Cells[7].Value.ToString();
            txt_methodName.Text = dataGridView.Rows[e.RowIndex].Cells[8].Value.ToString();
            txt_lineNumber.Text = dataGridView.Rows[e.RowIndex].Cells[9].Value.ToString();
            txt_codeBlock.Text = dataGridView.Rows[e.RowIndex].Cells[10].Value.ToString();
            txt_bugDetails.Text = dataGridView.Rows[e.RowIndex].Cells[11].Value.ToString();
            pictureBox.Image = ByteToImage((byte[])dataGridView.Rows[e.RowIndex].Cells[12].Value);
        }


        private void txt_codeBlock_Load(object sender, EventArgs e)
        {
            string cBlock = Application.StartupPath;
            FileSyntaxModeProvider fsmp;
            if (Directory.Exists(cBlock))
            {
                fsmp = new FileSyntaxModeProvider(cBlock);
                HighlightingManager.Manager.AddSyntaxModeFileProvider(fsmp);
                txt_codeBlock.SetHighlighting("C#");
            }
        }


        private void newBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewBug nbug = new NewBug(txt_fEmail.Text, cmb_fRole.Text);
            nbug.Show();
        }

        private void bugListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            displayData();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to close? ", "Bug Tracking Application", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                this.Close();
        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            {
                DataView dv = new DataView(dt);
                dv.RowFilter = string.Format("email LIKE '%{0}'", txt_search.Text);
                dv.RowFilter = string.Format("role LIKE '%{0}'", txt_search.Text);
                dv.RowFilter = string.Format("bug_id LIKE '%{0}'", txt_search.Text);
                dv.RowFilter = string.Format("project_title LIKE '%{0}'", txt_search.Text);
                dv.RowFilter = string.Format("entry_date LIKE '%{0}'", txt_search.Text);
                dataGridView.DataSource = dv;
            }
        }
    }
}
