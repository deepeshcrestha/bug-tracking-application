﻿namespace Bug_Tracking_Application
{
    partial class History
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.txt_search = new System.Windows.Forms.TextBox();
            this.txt_codeBlock = new ICSharpCode.TextEditor.TextEditorControl();
            this.lbl_image = new System.Windows.Forms.Label();
            this.txt_fDate = new System.Windows.Forms.TextBox();
            this.txt_fEmail = new System.Windows.Forms.TextBox();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.cmb_severity = new System.Windows.Forms.ComboBox();
            this.cmb_fRole = new System.Windows.Forms.ComboBox();
            this.cmb_role = new System.Windows.Forms.ComboBox();
            this.txt_bugId = new System.Windows.Forms.TextBox();
            this.txt_className = new System.Windows.Forms.TextBox();
            this.txt_project = new System.Windows.Forms.TextBox();
            this.txt_bugDetails = new System.Windows.Forms.TextBox();
            this.txt_lineNumber = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txt_methodName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lbl_serverity = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelPic = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_email = new System.Windows.Forms.Label();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.dateTimePicker);
            this.panel1.Controls.Add(this.txt_search);
            this.panel1.Controls.Add(this.txt_codeBlock);
            this.panel1.Controls.Add(this.lbl_image);
            this.panel1.Controls.Add(this.txt_fDate);
            this.panel1.Controls.Add(this.txt_fEmail);
            this.panel1.Controls.Add(this.txt_email);
            this.panel1.Controls.Add(this.pictureBox);
            this.panel1.Controls.Add(this.cmb_severity);
            this.panel1.Controls.Add(this.cmb_fRole);
            this.panel1.Controls.Add(this.cmb_role);
            this.panel1.Controls.Add(this.txt_bugId);
            this.panel1.Controls.Add(this.txt_className);
            this.panel1.Controls.Add(this.txt_project);
            this.panel1.Controls.Add(this.txt_bugDetails);
            this.panel1.Controls.Add(this.txt_lineNumber);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.txt_methodName);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.lbl_serverity);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.labelPic);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(4, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1347, 399);
            this.panel1.TabIndex = 12;
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.CustomFormat = "yyyy-MM-dd H:mm:ss";
            this.dateTimePicker.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker.Location = new System.Drawing.Point(417, 55);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(244, 29);
            this.dateTimePicker.TabIndex = 26;
            // 
            // txt_search
            // 
            this.txt_search.Location = new System.Drawing.Point(667, 360);
            this.txt_search.Multiline = true;
            this.txt_search.Name = "txt_search";
            this.txt_search.Size = new System.Drawing.Size(330, 31);
            this.txt_search.TabIndex = 6;
            this.txt_search.TextChanged += new System.EventHandler(this.txt_search_TextChanged);
            // 
            // txt_codeBlock
            // 
            this.txt_codeBlock.IsReadOnly = false;
            this.txt_codeBlock.Location = new System.Drawing.Point(12, 178);
            this.txt_codeBlock.Name = "txt_codeBlock";
            this.txt_codeBlock.Size = new System.Drawing.Size(588, 154);
            this.txt_codeBlock.TabIndex = 25;
            this.txt_codeBlock.Load += new System.EventHandler(this.txt_codeBlock_Load);
            // 
            // lbl_image
            // 
            this.lbl_image.AutoSize = true;
            this.lbl_image.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_image.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lbl_image.Location = new System.Drawing.Point(1070, 336);
            this.lbl_image.Name = "lbl_image";
            this.lbl_image.Size = new System.Drawing.Size(0, 22);
            this.lbl_image.TabIndex = 24;
            // 
            // txt_fDate
            // 
            this.txt_fDate.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_fDate.Location = new System.Drawing.Point(754, 13);
            this.txt_fDate.Multiline = true;
            this.txt_fDate.Name = "txt_fDate";
            this.txt_fDate.ReadOnly = true;
            this.txt_fDate.Size = new System.Drawing.Size(243, 29);
            this.txt_fDate.TabIndex = 3;
            // 
            // txt_fEmail
            // 
            this.txt_fEmail.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_fEmail.Location = new System.Drawing.Point(92, 13);
            this.txt_fEmail.Multiline = true;
            this.txt_fEmail.Name = "txt_fEmail";
            this.txt_fEmail.ReadOnly = true;
            this.txt_fEmail.Size = new System.Drawing.Size(243, 29);
            this.txt_fEmail.TabIndex = 1;
            // 
            // txt_email
            // 
            this.txt_email.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_email.Location = new System.Drawing.Point(1103, 12);
            this.txt_email.Multiline = true;
            this.txt_email.Name = "txt_email";
            this.txt_email.ReadOnly = true;
            this.txt_email.Size = new System.Drawing.Size(237, 30);
            this.txt_email.TabIndex = 1;
            // 
            // pictureBox
            // 
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox.Location = new System.Drawing.Point(1004, 178);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(336, 154);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 22;
            this.pictureBox.TabStop = false;
            // 
            // cmb_severity
            // 
            this.cmb_severity.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_severity.FormattingEnabled = true;
            this.cmb_severity.Items.AddRange(new object[] {
            "Critical",
            "Enhancement",
            "Major",
            "Minor",
            "Normal",
            "Trival"});
            this.cmb_severity.Location = new System.Drawing.Point(92, 106);
            this.cmb_severity.Name = "cmb_severity";
            this.cmb_severity.Size = new System.Drawing.Size(243, 30);
            this.cmb_severity.TabIndex = 6;
            // 
            // cmb_fRole
            // 
            this.cmb_fRole.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_fRole.FormattingEnabled = true;
            this.cmb_fRole.Items.AddRange(new object[] {
            "Developer",
            "Programmer",
            "Tester"});
            this.cmb_fRole.Location = new System.Drawing.Point(417, 12);
            this.cmb_fRole.Name = "cmb_fRole";
            this.cmb_fRole.Size = new System.Drawing.Size(243, 30);
            this.cmb_fRole.TabIndex = 2;
            // 
            // cmb_role
            // 
            this.cmb_role.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_role.FormattingEnabled = true;
            this.cmb_role.Items.AddRange(new object[] {
            "Developer",
            "Programmer",
            "Tester"});
            this.cmb_role.Location = new System.Drawing.Point(92, 56);
            this.cmb_role.Name = "cmb_role";
            this.cmb_role.Size = new System.Drawing.Size(243, 30);
            this.cmb_role.TabIndex = 2;
            // 
            // txt_bugId
            // 
            this.txt_bugId.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_bugId.Location = new System.Drawing.Point(1103, 56);
            this.txt_bugId.Multiline = true;
            this.txt_bugId.Name = "txt_bugId";
            this.txt_bugId.Size = new System.Drawing.Size(237, 30);
            this.txt_bugId.TabIndex = 5;
            // 
            // txt_className
            // 
            this.txt_className.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_className.Location = new System.Drawing.Point(418, 104);
            this.txt_className.Multiline = true;
            this.txt_className.Name = "txt_className";
            this.txt_className.Size = new System.Drawing.Size(243, 28);
            this.txt_className.TabIndex = 7;
            // 
            // txt_project
            // 
            this.txt_project.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_project.Location = new System.Drawing.Point(754, 56);
            this.txt_project.Multiline = true;
            this.txt_project.Name = "txt_project";
            this.txt_project.Size = new System.Drawing.Size(243, 30);
            this.txt_project.TabIndex = 4;
            // 
            // txt_bugDetails
            // 
            this.txt_bugDetails.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_bugDetails.Location = new System.Drawing.Point(606, 178);
            this.txt_bugDetails.Multiline = true;
            this.txt_bugDetails.Name = "txt_bugDetails";
            this.txt_bugDetails.Size = new System.Drawing.Size(391, 154);
            this.txt_bugDetails.TabIndex = 11;
            // 
            // txt_lineNumber
            // 
            this.txt_lineNumber.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_lineNumber.Location = new System.Drawing.Point(1103, 104);
            this.txt_lineNumber.Multiline = true;
            this.txt_lineNumber.Name = "txt_lineNumber";
            this.txt_lineNumber.Size = new System.Drawing.Size(237, 28);
            this.txt_lineNumber.TabIndex = 9;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(339, 17);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(75, 18);
            this.label14.TabIndex = 10;
            this.label14.Text = "Fixer Role *";
            // 
            // txt_methodName
            // 
            this.txt_methodName.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_methodName.Location = new System.Drawing.Point(754, 104);
            this.txt_methodName.Multiline = true;
            this.txt_methodName.Name = "txt_methodName";
            this.txt_methodName.Size = new System.Drawing.Size(243, 28);
            this.txt_methodName.TabIndex = 8;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(664, 17);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 18);
            this.label13.TabIndex = 11;
            this.label13.Text = "Fixed Date *";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 18);
            this.label1.TabIndex = 10;
            this.label1.Text = "Assignee Role *";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(339, 62);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 18);
            this.label11.TabIndex = 11;
            this.label11.Text = "Entry Date *";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(733, 158);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 18);
            this.label8.TabIndex = 3;
            this.label8.Text = "Bug Description *";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(664, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 18);
            this.label6.TabIndex = 4;
            this.label6.Text = "Method Name";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1001, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 18);
            this.label9.TabIndex = 5;
            this.label9.Text = "Bug ID *";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(249, 158);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 18);
            this.label12.TabIndex = 5;
            this.label12.Text = "Code Block";
            // 
            // lbl_serverity
            // 
            this.lbl_serverity.AutoSize = true;
            this.lbl_serverity.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_serverity.Location = new System.Drawing.Point(9, 112);
            this.lbl_serverity.Name = "lbl_serverity";
            this.lbl_serverity.Size = new System.Drawing.Size(66, 18);
            this.lbl_serverity.TabIndex = 5;
            this.lbl_serverity.Text = "Severity *";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(339, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 18);
            this.label5.TabIndex = 5;
            this.label5.Text = "Class Name";
            // 
            // labelPic
            // 
            this.labelPic.AutoSize = true;
            this.labelPic.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPic.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labelPic.Location = new System.Drawing.Point(25, 106);
            this.labelPic.Name = "labelPic";
            this.labelPic.Size = new System.Drawing.Size(0, 22);
            this.labelPic.TabIndex = 6;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(605, 363);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 22);
            this.label15.TabIndex = 6;
            this.label15.Text = "Search";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(1001, 154);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 18);
            this.label10.TabIndex = 6;
            this.label10.Text = "Attachment";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 18);
            this.label2.TabIndex = 8;
            this.label2.Text = "Fixer Email *";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(1001, 109);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 18);
            this.label7.TabIndex = 7;
            this.label7.Text = "Line Number";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(999, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 18);
            this.label3.TabIndex = 8;
            this.label3.Text = "Assignee Email *";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(664, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 18);
            this.label4.TabIndex = 9;
            this.label4.Text = "Project Titile *";
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_email.Location = new System.Drawing.Point(810, -5);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(0, 22);
            this.lbl_email.TabIndex = 14;
            // 
            // dataGridView
            // 
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Location = new System.Drawing.Point(4, 424);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.Size = new System.Drawing.Size(1347, 239);
            this.dataGridView.TabIndex = 13;
            this.dataGridView.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_RowHeaderMouseClick);
            // 
            // History
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1352, 665);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lbl_email);
            this.Controls.Add(this.dataGridView);
            this.Name = "History";
            this.Text = "History";
            this.Load += new System.EventHandler(this.History_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.TextBox txt_search;
        private ICSharpCode.TextEditor.TextEditorControl txt_codeBlock;
        private System.Windows.Forms.Label lbl_image;
        private System.Windows.Forms.TextBox txt_fDate;
        private System.Windows.Forms.TextBox txt_fEmail;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.ComboBox cmb_severity;
        private System.Windows.Forms.ComboBox cmb_fRole;
        private System.Windows.Forms.ComboBox cmb_role;
        private System.Windows.Forms.TextBox txt_bugId;
        private System.Windows.Forms.TextBox txt_className;
        private System.Windows.Forms.TextBox txt_project;
        private System.Windows.Forms.TextBox txt_bugDetails;
        private System.Windows.Forms.TextBox txt_lineNumber;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txt_methodName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbl_serverity;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelPic;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.DataGridView dataGridView;
    }
}