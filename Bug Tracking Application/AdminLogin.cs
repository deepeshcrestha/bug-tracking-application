﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Bug_Tracking_Application
{
    public partial class AdminLogin : Form
    {
        MySqlConnection mySQLCon = new MySqlConnection("server= localhost; port = 3306; database= bug_tracker; user id =root ; password = ;SslMode=none");
        int i;
        public AdminLogin()
        {
            InitializeComponent();
        }
        private void ClearData()
        {
            txt_Username.Text = "Username";
            txt_Password.Text = "Password";
        }

        private void btn_Login_Click(object sender, EventArgs e)
        {
            i = 0;
            mySQLCon.Open();
            MySqlCommand cmd = mySQLCon.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from login where role='" + cmb_Role.Text + "' and email='" + txt_Username.Text + "' and password='" + txt_Password.Text + "'";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            i = Convert.ToInt32(dt.Rows.Count.ToString());
            if (i == 0)
            {
                label3.Text = "Invalid Username or Password or Role";
                ClearData();
            }
            else
            {

                MessageBox.Show("Login Successful ", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
                AdminDashboard adb = new AdminDashboard(txt_Username.Text, cmb_Role.Text);
                adb.Show();
            }
            mySQLCon.Close();
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to close? ", "Bug Tracking Application", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                this.Close();
        }

        private void AdminLogin_Load(object sender, EventArgs e)
        {
            this.ActiveControl = label1;
            txt_Username.Text = "Username";
            txt_Password.Text = "Password";
        }

        //Empties the username textbox once it's focused
        private void txt_Username_Enter(object sender, EventArgs e)
        {
            if (txt_Username.Focus())
                txt_Username.Text = string.Empty;
        }

        //Resets the placeholder text for username textbox
        private void txt_Username_Leave(object sender, EventArgs e)
        {
            if (!txt_Username.Focused && txt_Username.Text.Trim() == string.Empty)
                txt_Username.Text = "Username";
        }

        //Empties the password textbox once it's focused
        private void txt_Password_Enter(object sender, EventArgs e)
        {
            if (txt_Password.Focus())
                txt_Password.Text = string.Empty;
        }

        //Resets the placeholder text for password textbox
        private void txt_Password_Leave(object sender, EventArgs e)
        {
            if (!txt_Password.Focused && txt_Password.Text.Trim() == string.Empty)
                txt_Password.Text = "Password";
        }

        private void cmb_Role_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;

        }
    }
}
