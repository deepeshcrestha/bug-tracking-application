﻿namespace Bug_Tracking_Application
{
    partial class NewBug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txt_codeBlock = new ICSharpCode.TextEditor.TextEditorControl();
            this.lbl_image = new System.Windows.Forms.Label();
            this.txt_date = new System.Windows.Forms.TextBox();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_Upload = new System.Windows.Forms.Button();
            this.cmb_severity = new System.Windows.Forms.ComboBox();
            this.cmb_role = new System.Windows.Forms.ComboBox();
            this.txt_bugId = new System.Windows.Forms.TextBox();
            this.txt_className = new System.Windows.Forms.TextBox();
            this.txt_project = new System.Windows.Forms.TextBox();
            this.txt_bugDetails = new System.Windows.Forms.TextBox();
            this.txt_lineNumber = new System.Windows.Forms.TextBox();
            this.txt_methodName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lbl_serverity = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelPic = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.newProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutBugTrackingApplicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.txt_codeBlock);
            this.panel1.Controls.Add(this.lbl_image);
            this.panel1.Controls.Add(this.txt_date);
            this.panel1.Controls.Add(this.txt_email);
            this.panel1.Controls.Add(this.pictureBox);
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.btn_save);
            this.panel1.Controls.Add(this.btn_Upload);
            this.panel1.Controls.Add(this.cmb_severity);
            this.panel1.Controls.Add(this.cmb_role);
            this.panel1.Controls.Add(this.txt_bugId);
            this.panel1.Controls.Add(this.txt_className);
            this.panel1.Controls.Add(this.txt_project);
            this.panel1.Controls.Add(this.txt_bugDetails);
            this.panel1.Controls.Add(this.txt_lineNumber);
            this.panel1.Controls.Add(this.txt_methodName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.lbl_serverity);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.labelPic);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(3, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1134, 417);
            this.panel1.TabIndex = 0;
           // this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // txt_codeBlock
            // 
            this.txt_codeBlock.IsReadOnly = false;
            this.txt_codeBlock.Location = new System.Drawing.Point(12, 187);
            this.txt_codeBlock.Name = "txt_codeBlock";
            this.txt_codeBlock.Size = new System.Drawing.Size(339, 154);
            this.txt_codeBlock.TabIndex = 10;
            this.txt_codeBlock.Load += new System.EventHandler(this.txt_codeBlock_Load);
            // 
            // lbl_image
            // 
            this.lbl_image.AutoSize = true;
            this.lbl_image.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_image.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lbl_image.Location = new System.Drawing.Point(855, 345);
            this.lbl_image.Name = "lbl_image";
            this.lbl_image.Size = new System.Drawing.Size(0, 22);
            this.lbl_image.TabIndex = 24;
            // 
            // txt_date
            // 
            this.txt_date.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_date.Location = new System.Drawing.Point(882, 20);
            this.txt_date.Multiline = true;
            this.txt_date.Name = "txt_date";
            this.txt_date.ReadOnly = true;
            this.txt_date.Size = new System.Drawing.Size(243, 29);
            this.txt_date.TabIndex = 3;
           // this.txt_date.TextChanged += new System.EventHandler(this.txt_date_TextChanged);
            // 
            // txt_email
            // 
            this.txt_email.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_email.Location = new System.Drawing.Point(108, 21);
            this.txt_email.Multiline = true;
            this.txt_email.Name = "txt_email";
            this.txt_email.ReadOnly = true;
            this.txt_email.Size = new System.Drawing.Size(243, 29);
            this.txt_email.TabIndex = 1;
            // 
            // pictureBox
            // 
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox.Location = new System.Drawing.Point(769, 187);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(356, 154);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 22;
            this.pictureBox.TabStop = false;
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.Gray;
            this.btn_close.Location = new System.Drawing.Point(606, 369);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(99, 33);
            this.btn_close.TabIndex = 14;
            this.btn_close.Text = "Close";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_save
            // 
            this.btn_save.BackColor = System.Drawing.Color.Gray;
            this.btn_save.Location = new System.Drawing.Point(438, 369);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(99, 33);
            this.btn_save.TabIndex = 13;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = false;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_Upload
            // 
            this.btn_Upload.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Upload.Location = new System.Drawing.Point(882, 157);
            this.btn_Upload.Name = "btn_Upload";
            this.btn_Upload.Size = new System.Drawing.Size(243, 28);
            this.btn_Upload.TabIndex = 12;
            this.btn_Upload.Text = "Upload Image";
            this.btn_Upload.UseVisualStyleBackColor = true;
            this.btn_Upload.Click += new System.EventHandler(this.btn_Upload_Click);
            // 
            // cmb_severity
            // 
            this.cmb_severity.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_severity.FormattingEnabled = true;
            this.cmb_severity.Items.AddRange(new object[] {
            "Critical",
            "Enhancement",
            "Major",
            "Minor",
            "Normal",
            "Trival"});
            this.cmb_severity.Location = new System.Drawing.Point(882, 67);
            this.cmb_severity.Name = "cmb_severity";
            this.cmb_severity.Size = new System.Drawing.Size(243, 30);
            this.cmb_severity.TabIndex = 6;
            this.cmb_severity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmb_severity_KeyPress);
            // 
            // cmb_role
            // 
            this.cmb_role.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_role.FormattingEnabled = true;
            this.cmb_role.Items.AddRange(new object[] {
            "Developer",
            "Programmer",
            "Tester"});
            this.cmb_role.Location = new System.Drawing.Point(496, 21);
            this.cmb_role.Name = "cmb_role";
            this.cmb_role.Size = new System.Drawing.Size(243, 30);
            this.cmb_role.TabIndex = 2;
            this.cmb_role.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmb_role_KeyPress);
            // 
            // txt_bugId
            // 
            this.txt_bugId.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_bugId.Location = new System.Drawing.Point(496, 69);
            this.txt_bugId.Multiline = true;
            this.txt_bugId.Name = "txt_bugId";
            this.txt_bugId.Size = new System.Drawing.Size(243, 29);
            this.txt_bugId.TabIndex = 5;
            // 
            // txt_className
            // 
            this.txt_className.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_className.Location = new System.Drawing.Point(108, 114);
            this.txt_className.Multiline = true;
            this.txt_className.Name = "txt_className";
            this.txt_className.Size = new System.Drawing.Size(243, 28);
            this.txt_className.TabIndex = 7;
            // 
            // txt_project
            // 
            this.txt_project.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_project.Location = new System.Drawing.Point(108, 69);
            this.txt_project.Multiline = true;
            this.txt_project.Name = "txt_project";
            this.txt_project.Size = new System.Drawing.Size(243, 28);
            this.txt_project.TabIndex = 4;
            // 
            // txt_bugDetails
            // 
            this.txt_bugDetails.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_bugDetails.Location = new System.Drawing.Point(383, 187);
            this.txt_bugDetails.Multiline = true;
            this.txt_bugDetails.Name = "txt_bugDetails";
            this.txt_bugDetails.Size = new System.Drawing.Size(356, 154);
            this.txt_bugDetails.TabIndex = 11;
            //this.txt_bugDetails.TextChanged += new System.EventHandler(this.txt_bugDetails_TextChanged);
            // 
            // txt_lineNumber
            // 
            this.txt_lineNumber.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_lineNumber.Location = new System.Drawing.Point(882, 111);
            this.txt_lineNumber.Multiline = true;
            this.txt_lineNumber.Name = "txt_lineNumber";
            this.txt_lineNumber.Size = new System.Drawing.Size(243, 28);
            this.txt_lineNumber.TabIndex = 9;
            //this.txt_lineNumber.TextChanged += new System.EventHandler(this.txt_lineNumber_TextChanged);
            // 
            // txt_methodName
            // 
            this.txt_methodName.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_methodName.Location = new System.Drawing.Point(496, 114);
            this.txt_methodName.Multiline = true;
            this.txt_methodName.Name = "txt_methodName";
            this.txt_methodName.Size = new System.Drawing.Size(243, 28);
            this.txt_methodName.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(379, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 22);
            this.label1.TabIndex = 10;
            this.label1.Text = "Role *";
           // this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(763, 18);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 22);
            this.label11.TabIndex = 11;
            this.label11.Text = "Entry Date *";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(379, 152);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 22);
            this.label8.TabIndex = 3;
            this.label8.Text = "Bug Description *";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(379, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 22);
            this.label6.TabIndex = 4;
            this.label6.Text = "Method Name :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(379, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 22);
            this.label9.TabIndex = 5;
            this.label9.Text = "Bug ID *";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 152);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 22);
            this.label12.TabIndex = 5;
            this.label12.Text = "Code Block :";
            // 
            // lbl_serverity
            // 
            this.lbl_serverity.AutoSize = true;
            this.lbl_serverity.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_serverity.Location = new System.Drawing.Point(763, 69);
            this.lbl_serverity.Name = "lbl_serverity";
            this.lbl_serverity.Size = new System.Drawing.Size(75, 22);
            this.lbl_serverity.TabIndex = 5;
            this.lbl_serverity.Text = "Severity *";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 114);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 22);
            this.label5.TabIndex = 5;
            this.label5.Text = "Class Name :";
            // 
            // labelPic
            // 
            this.labelPic.AutoSize = true;
            this.labelPic.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPic.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labelPic.Location = new System.Drawing.Point(25, 106);
            this.labelPic.Name = "labelPic";
            this.labelPic.Size = new System.Drawing.Size(0, 22);
            this.labelPic.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(765, 160);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 22);
            this.label10.TabIndex = 6;
            this.label10.Text = "Attachment :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(763, 114);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 22);
            this.label7.TabIndex = 7;
            this.label7.Text = "Line Number :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 22);
            this.label3.TabIndex = 8;
            this.label3.Text = "Email  *";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 22);
            this.label4.TabIndex = 9;
            this.label4.Text = "Project Titile *";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newProjectToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1141, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // newProjectToolStripMenuItem
            // 
            this.newProjectToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.newProjectToolStripMenuItem.Name = "newProjectToolStripMenuItem";
            this.newProjectToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.newProjectToolStripMenuItem.Text = "File";
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.clearToolStripMenuItem.Text = "Clear All Fields";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.closeToolStripMenuItem.Text = "Close";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewHelpToolStripMenuItem,
            this.aboutBugTrackingApplicationToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // viewHelpToolStripMenuItem
            // 
            this.viewHelpToolStripMenuItem.Name = "viewHelpToolStripMenuItem";
            this.viewHelpToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.viewHelpToolStripMenuItem.Text = "View Help";
            // 
            // aboutBugTrackingApplicationToolStripMenuItem
            // 
            this.aboutBugTrackingApplicationToolStripMenuItem.Name = "aboutBugTrackingApplicationToolStripMenuItem";
            this.aboutBugTrackingApplicationToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.aboutBugTrackingApplicationToolStripMenuItem.Text = "About Bug Tracking Application";
            // 
            // NewBug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1141, 445);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "NewBug";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Bug";
            this.Load += new System.EventHandler(this.NewBug_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmb_role;
        private System.Windows.Forms.TextBox txt_className;
        private System.Windows.Forms.TextBox txt_project;
        private System.Windows.Forms.TextBox txt_bugDetails;
        private System.Windows.Forms.TextBox txt_lineNumber;
        private System.Windows.Forms.TextBox txt_methodName;
        private System.Windows.Forms.Button btn_Upload;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Label labelPic;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txt_bugId;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txt_date;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.Label lbl_image;
        private System.Windows.Forms.ComboBox cmb_severity;
        private System.Windows.Forms.Label lbl_serverity;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem newProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutBugTrackingApplicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private ICSharpCode.TextEditor.TextEditorControl txt_codeBlock;
    }
}