﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Bug_Tracking_Application
{
    public partial class UserDashboard : Form
    {
        MySqlConnection con = new MySqlConnection("server= localhost; port = 3306; database= bug_tracker; user id =root ; password = ;SslMode=none;convert zero datetime=True;");
        MySqlCommand cmd;

        public UserDashboard(string str_email, string str_role)
        {
            InitializeComponent();
            lbl_email.Text = str_email;
            lbl_role.Text = str_role;

        }

        private void newBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewBug nbug = new NewBug(lbl_email.Text, lbl_role.Text);
            nbug.Show();
        }

        private void bugListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BugInformation binfo = new BugInformation(lbl_email.Text, lbl_role.Text);
           // binfo.MdiParent = this;
            if (lbl_role.Text == "Admin")
            {
                binfo.btn_delete.Enabled = true;
            }
            else
            {
                binfo.btn_delete.Enabled = false;
            }
            binfo.Show();
        }

        private void linklbl_versionControl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linklbl_versionControl.LinkVisited = true;

            System.Diagnostics.Process.Start("https://bitbucket.org/deepeshcrestha/bug-tracking-application/src/master/");
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to close? ", "Bug Tracking Application", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                this.Close();
        }

        private void viewHelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Help help = new Help();
            help.Show();
        }

        private void bugHistoryToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            History his = new History();
            his.Show();
        }
    }
}
