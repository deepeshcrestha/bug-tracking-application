﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bug_Tracking_Application
{
    public partial class AdminDashboard : Form
    {
        public AdminDashboard(string str_email, string str_role)
        {
            InitializeComponent();
            lbl_email.Text = str_email;
            lbl_role.Text = str_role;
        }

        private void newBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewBug nba = new NewBug(lbl_email.Text, lbl_role.Text);
            nba.Show();
        }

        private void userListToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            UserData ud = new UserData();
            ud.Show();
        }

        private void bugListToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            BugInformation bdata = new BugInformation(lbl_email.Text, lbl_role.Text);
            //bdata.MdiParent = this;
            //if (lbl_role.Text == "Admin")
            //{
            //    bdata.btn_delete.Enabled = true;
            //}
            //else
            //{
            //    bdata.btn_delete.Enabled = false;
            //}
            bdata.Show();

        }

        private void newUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SignUp su = new SignUp();
            su.Show();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to close? ", "Bug Tracking Application", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                this.Close();
        }

        private void viewHelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Help help = new Help();
            help.Show();
        }

        private void bugHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            History his = new History();
            his.Show();
        }
    }
}
