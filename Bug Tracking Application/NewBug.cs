﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
//using ICSharpCode.TextEditor.Document;
using System.IO;
using ICSharpCode.TextEditor.Document;
using System.Text.RegularExpressions;

namespace Bug_Tracking_Application
{
    public partial class NewBug : Form
    {
        MySqlConnection con = new MySqlConnection("server = localhost; port = 3306; database = bug_tracker; user id = root; password = ; SslMode = none; Integrated Security = true; convert zero datetime = True");
        MySqlCommand cmd;
        Image file;
        DateTime dt;

        //initialize the validating method
        static Regex Valid_LineNum = NumbersOnly();

        //Method for numbers validation only
        private static Regex NumbersOnly()
        {
            string StringAndNumber_Pattern = "^[0-9]*$";

            return new Regex(StringAndNumber_Pattern, RegexOptions.IgnoreCase);
        }



        public NewBug(string str_email, string str_role)
        {
            InitializeComponent();
            txt_email.Text = str_email;
            cmb_role.Text = str_role;
            txt_date.Text = DateTime.Now.ToString();

        }


        protected void TextBoxStartDate_TextChanged(object sender, EventArgs e)
        {

            // txt_date.Text = DateTime.Now.ToString("dd-MM-yyyy");
            //txt_date.Text = DateTime.Now();
        }
        private void btn_Upload_Click(object sender, EventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();
            f.Filter = "JPG(*.JPG)|*.jpg";
            if (f.ShowDialog() == DialogResult.OK)
            {
                file = Image.FromFile(f.FileName);
                pictureBox.Image = file;
                lbl_image.Text = "Image Uploaded Successfully";
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to close? ", "Bug Tracking Application", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                this.Close();
        }
        public static byte[] converterDemo(Image x)
        {
            ImageConverter _imageConverter = new ImageConverter();
            byte[] xByte = (byte[])_imageConverter.ConvertTo(x, typeof(byte[]));
            return xByte;
        }

        private void clearData()
        {
            txt_project.Text = "";
            txt_bugId.Text = "";
            cmb_severity.Text = "";
            txt_className.Text = "";
            txt_methodName.Text = "";
            txt_lineNumber.Text = "";
            txt_codeBlock.Text = "";
            txt_bugDetails.Text = "";
        }

        private void btn_save_Click(object sender, EventArgs e)
        {

            var imagetobyte = converterDemo(file);
            //for Contacts 
            if (Valid_LineNum.IsMatch(txt_lineNumber.Text) != true)
            {
                MessageBox.Show("Line number accept numbers only.", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txt_lineNumber.Focus();
                return;
            }

            if (txt_email.Text != "" && cmb_role.Text != "" && txt_date.Text != "" && txt_project.Text != "" && txt_bugId.Text != "" && cmb_severity.Text != "" && txt_bugDetails.Text != "")
            {
                cmd = new MySqlCommand("insert into register_bug (email, role, entry_date, project_title, bug_id, severity, class_name, method_name, line_number, code_block, bug_description, image) " +
                    "values (@email, @role, @date, @project_title, @bug_id, @severity, @class_name, @method_name, @line_number, @code_block, @bug_description, @image)", con);
                con.Open();
                cmd.Parameters.AddWithValue("@email", txt_email.Text);
                cmd.Parameters.AddWithValue("@role", cmb_role.Text);
                cmd.Parameters.AddWithValue("@date", DateTime.Now.ToString("yyyy-MM-dd H:mm:ss"));
                cmd.Parameters.AddWithValue("@project_title", txt_project.Text);
                cmd.Parameters.AddWithValue("@bug_id", txt_bugId.Text);
                cmd.Parameters.AddWithValue("@severity", cmb_severity.Text);
                cmd.Parameters.AddWithValue("@class_name", txt_className.Text);
                cmd.Parameters.AddWithValue("@method_name", txt_methodName.Text);
                cmd.Parameters.AddWithValue("@line_number", txt_lineNumber.Text);
                cmd.Parameters.AddWithValue("@code_block", txt_codeBlock.Text);
                cmd.Parameters.AddWithValue("@bug_description", txt_bugDetails.Text);
                // cmd.Parameters.AddWithValue("@image", pictureBox.Image);
                var paramUserImage = new MySqlParameter("@image", MySqlDbType.Blob, imagetobyte.Length);
                paramUserImage.Value = imagetobyte;
                cmd.Parameters.Add(paramUserImage);
                cmd.ExecuteNonQuery();
                clearData();
                con.Close();
                MessageBox.Show("Record Inserted Successfully.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Please Provide All Details.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void NewBug_Load(object sender, EventArgs e)
        {
            this.txt_date.Text = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss");
        }

        private void txt_codeBlock_Load(object sender, EventArgs e)
        {
            string cBlock = Application.StartupPath;
            FileSyntaxModeProvider fsmp;
            if (Directory.Exists(cBlock))
            {
                fsmp = new FileSyntaxModeProvider(cBlock);
                HighlightingManager.Manager.AddSyntaxModeFileProvider(fsmp);
                txt_codeBlock.SetHighlighting("C#");
            }
        }

        private void cmb_role_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cmb_severity_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
    }
}
