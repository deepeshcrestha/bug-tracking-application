﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Bug_Tracking_Application
{
    public partial class UserData : Form
    {

        MySqlConnection con = new MySqlConnection("server= localhost; port = 3306; database= bug_tracker; user id =root ; password = ;SslMode=none;convert zero datetime=True");
        MySqlCommand cmd;
        MySqlDataAdapter adapt;
        MySqlDataReader mdr;
        int id = 0;
        DataTable dt;

        public UserData()
        {
            InitializeComponent();
        }
        

        //initialize the validating method
        static Regex Valid_Name = StringOnly();
        //static Regex Valid_Contact = NumbersOnly();
        static Regex Valid_Password = ValidPassword();
        static Regex Valid_Email = Email_Address();

        //Method for validating email address
        private static Regex Email_Address()
        {
            string Email_Pattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            return new Regex(Email_Pattern, RegexOptions.IgnoreCase);
        }

        //Method for string validation only
        private static Regex StringOnly()
        {
            string StringAndNumber_Pattern = "^[a-zA-Z]";

            return new Regex(StringAndNumber_Pattern, RegexOptions.IgnoreCase);
        }


        //Method for password validation only
        private static Regex ValidPassword()
        {
            string Password_Pattern = "(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,15})$";

            return new Regex(Password_Pattern, RegexOptions.IgnoreCase);
        }



        public void displayData()
        {
            con.Open();
            MySqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Select * from user_data";
            cmd.ExecuteNonQuery();
            dt = new DataTable();
            MySqlDataAdapter adpt = new MySqlDataAdapter(cmd);
            adpt.Fill(dt);
            dataGridView.DataSource = dt;
            con.Close();
        }
        private void clearData()
        {
            txt_firstname.Text = "";
            txt_lastname.Text = "";
            txt_address.Text = "";
            cmb_role.Text = "";
            txt_email.Text = "";
            txt_password.Text = "";

        }

        private void UserData_Load(object sender, EventArgs e)
        {
            displayData();
            //txt_search.Text = "Search";

        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            // for firstname if this field is empty
            if (txt_firstname.Text == "")
            {
                MessageBox.Show("Firstname cannot be empty.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txt_firstname.Focus();
                return;
            }

            //for firstname if this field contains numeric characters
            if (Valid_Name.IsMatch(txt_firstname.Text) != true)
            {
                MessageBox.Show("Firstname accepts only alphabetical characters.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txt_firstname.Focus();
                return;
            }

            //for lastname if this field is empty
            if (txt_lastname.Text == "")
            {
                MessageBox.Show("Lastname cannot be empty.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txt_lastname.Focus();
                return;
            }
            //for lastname if this field containd numeric characters
            if (Valid_Name.IsMatch(txt_lastname.Text) != true)
            {
                MessageBox.Show("Lastname accepts only alphabetical characters.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txt_lastname.Focus();
                return;
            }
            //for address if this field is empty
            if (txt_address.Text == "")
            {
                MessageBox.Show("Address cannot be empty.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txt_address.Focus();
                return;
            }
            //for role if this field is empty
            if (cmb_role.Text == "")
            {
                MessageBox.Show("Role cannot be empty.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                cmb_role.Focus();
                return;
            }
            // for email address if this field is empty
            if (txt_email.Text == "")
            {
                MessageBox.Show("Email cannot be empty.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txt_email.Focus();
                return;
            }
            //for Email Address if it is not valid 
            if (Valid_Email.IsMatch(txt_email.Text) != true)
            {
                MessageBox.Show("Invalid Email Address.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txt_email.Focus();
                return;
            }
            //for password if this field is empty
            if (txt_password.Text == "")
            {
                MessageBox.Show("Password cannot be empty.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txt_password.Focus();
                return;
            }

            //for password 
            if (Valid_Password.IsMatch(txt_password.Text) != true)
            {
                MessageBox.Show("Password must be atleast 8 to 15 characters and should contains atleast one Upper case and Numbers.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txt_password.Focus();
                return;
            }
            cmd = new MySqlCommand("update user_data set firstname=@firstname, lastname=@lastname, address=@address, role=@role, email=@email, password=@password where id=@id", con);
            con.Open();
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@firstname", txt_firstname.Text);
            cmd.Parameters.AddWithValue("@lastname", txt_lastname.Text);
            cmd.Parameters.AddWithValue("@address", txt_address.Text);
            cmd.Parameters.AddWithValue("@role", cmb_role.Text);
            cmd.Parameters.AddWithValue("@email", txt_email.Text);
            cmd.Parameters.AddWithValue("@password", txt_password.Text);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Record Updated Successfully.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Information);
            con.Close();
            displayData();
            clearData();

        }

        private void dataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            id = Convert.ToInt32(dataGridView.Rows[e.RowIndex].Cells[0].Value.ToString());
            txt_firstname.Text = dataGridView.Rows[e.RowIndex].Cells[1].Value.ToString();
            txt_lastname.Text = dataGridView.Rows[e.RowIndex].Cells[2].Value.ToString();
            txt_address.Text = dataGridView.Rows[e.RowIndex].Cells[3].Value.ToString();
            cmb_role.Text = dataGridView.Rows[e.RowIndex].Cells[4].Value.ToString();
            txt_email.Text = dataGridView.Rows[e.RowIndex].Cells[5].Value.ToString();
            txt_password.Text = dataGridView.Rows[e.RowIndex].Cells[6].Value.ToString();

        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (id != 0)
            {
                cmd = new MySqlCommand("delete from user_data where id=@id", con);
                con.Open();
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Record Deleted Successfully.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Information);
                displayData();
                clearData();
            }
            else
            {
                MessageBox.Show("Please Select Record to Delete.", "Bug Tracking Application", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            DataView dv = new DataView(dt);
            dv.RowFilter = string.Format("firstname LIKE '%{0}'", txt_search.Text);
            dv.RowFilter = string.Format("role LIKE '%{0}'", txt_search.Text);
            dataGridView.DataSource = dv;
        }

        private void userListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            displayData();
        }

    }
}
