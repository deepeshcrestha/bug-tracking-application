﻿using ICSharpCode.TextEditor.Document;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bug_Tracking_Application
{
    public partial class History : Form
    {
        public Image image;
        MySqlConnection con = new MySqlConnection("server= localhost; port = 3306; database= bug_tracker; user id =root ; password = ;SslMode=none;convert zero datetime=True;");
        MySqlCommand cmd;
        MySqlDataAdapter adapt;
        Image file;
        DataTable dt, dtable;
        int report_id = 0;

        public History()
        {
            InitializeComponent();
        }

        public void displayData()
        {
            string selectQuery = "select * from fixed_bug";
            cmd = new MySqlCommand(selectQuery, con);

            adapt = new MySqlDataAdapter(cmd);

            dt = new DataTable();
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView.RowTemplate.Height = 70;
            dataGridView.AllowUserToAddRows = false;

            adapt.Fill(dt);
            dataGridView.DataSource = dt;
            DataGridViewImageColumn imgcol = new DataGridViewImageColumn();
            imgcol = (DataGridViewImageColumn)dataGridView.Columns[15];
            imgcol.ImageLayout = DataGridViewImageCellLayout.Stretch;
            adapt.Dispose();
        }

        private void dataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            displayData();

            report_id = Convert.ToInt32(dataGridView.Rows[e.RowIndex].Cells[0].Value.ToString());
            txt_fEmail.Text = dataGridView.Rows[e.RowIndex].Cells[1].Value.ToString();
            cmb_fRole.Text = dataGridView.Rows[e.RowIndex].Cells[2].Value.ToString();
            txt_fDate.Text = dataGridView.Rows[e.RowIndex].Cells[3].Value.ToString();
            txt_email.Text = dataGridView.Rows[e.RowIndex].Cells[4].Value.ToString();
            cmb_role.Text = dataGridView.Rows[e.RowIndex].Cells[5].Value.ToString();
            this.dateTimePicker.Text = dataGridView.Rows[e.RowIndex].Cells[6].Value.ToString();
            txt_project.Text = dataGridView.Rows[e.RowIndex].Cells[7].Value.ToString();
            txt_bugId.Text = dataGridView.Rows[e.RowIndex].Cells[8].Value.ToString();
            cmb_severity.Text = dataGridView.Rows[e.RowIndex].Cells[9].Value.ToString();
            txt_className.Text = dataGridView.Rows[e.RowIndex].Cells[10].Value.ToString();
            txt_methodName.Text = dataGridView.Rows[e.RowIndex].Cells[11].Value.ToString();
            txt_lineNumber.Text = dataGridView.Rows[e.RowIndex].Cells[12].Value.ToString();
            txt_codeBlock.Text = dataGridView.Rows[e.RowIndex].Cells[12].Value.ToString();
            txt_bugDetails.Text = dataGridView.Rows[e.RowIndex].Cells[14].Value.ToString();
            pictureBox.Image = ByteToImage((byte[])dataGridView.Rows[e.RowIndex].Cells[15].Value);
        }

        private void txt_codeBlock_Load(object sender, EventArgs e)
        {
            string cBlock = Application.StartupPath;
            FileSyntaxModeProvider fsmp;
            if (Directory.Exists(cBlock))
            {
                fsmp = new FileSyntaxModeProvider(cBlock);
                HighlightingManager.Manager.AddSyntaxModeFileProvider(fsmp);
                txt_codeBlock.SetHighlighting("C#");
            }
        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            {
                DataView dv = new DataView(dt);
                dv.RowFilter = string.Format("email LIKE '%{0}'", txt_search.Text);
                dv.RowFilter = string.Format("role LIKE '%{0}'", txt_search.Text);
                dv.RowFilter = string.Format("bug_id LIKE '%{0}'", txt_search.Text);
                dv.RowFilter = string.Format("project_title LIKE '%{0}'", txt_search.Text);
                dv.RowFilter = string.Format("entry_date LIKE '%{0}'", txt_search.Text);
                dataGridView.DataSource = dv;
            }
        }

        private void History_Load(object sender, EventArgs e)
        {
            displayData();
        }

        public static Bitmap ByteToImage(byte[] blob)
        {
            MemoryStream mStream = new MemoryStream();
            byte[] pData = blob;
            mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
            Bitmap bm = new Bitmap(mStream, false);
            mStream.Dispose();
            return bm;
        }
    }

}
